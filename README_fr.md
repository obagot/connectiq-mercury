# Mercure #

## Description ##

Mercure est un cadran de montre pour les montres Garmin.

Il a été dessiné de façon à être clair et intuitif, et comprend la barre d'inactivité, l'état de la batterie, le nombre de pas, ainsi que des indicateurs pour la
connexion au téléphone, les alarmes et les notifications.

La couleur de l'heure et des minutes est configurable à partir de Garmin Connect Mobile.

La disposition configurable permet d'accentuer les heures ou les minutes.

Téléchargez-le sur la [Boutique Connect IQ](https://apps.garmin.com/fr-FR/apps/bd260ed0-21db-43f3-97f1-0e0c6fc38b9a) de Garmin.

Langues supportées : Anglais, Français, Italien, Espagnol.

Modèles supportés : vívoactive®, epix™, Forerunner® 920XT.

![Capture](dist/images/capture0.png)
![Capture](dist/images/capture5.png)
![Capture](dist/images/capture7.png)

## Versions ##

Mercure est testé sur Garmin vívoactive fonctionnant sur Connect IQ 1.2.3.

### v.1.17 - 2016-02-04

* Correction du chargement des préférences quand certaines options ne sont pas modifiées.

### v.1.16 - 2016-02-02

* Correction d'erreurs lors du chargement des préférences.

### v.1.15 - 2016-02-02

* Ajout d'options séparées pour la couleur de la date et des autres informations secondaires.
* Ajout d'une option pour cacher la date.
* Ajout d'une option pour précéder l'heure d'un zéro.
* Les barres d'activité et de pas ne sont plus affichées en mode sommeil.
* Modification des seuils colorés de la batterie : orange < 20%, rouge < 10%.
* Mise à jour vers Garmin Connect IQ SDK 1.2.3.

### v.1.14 - 2016-01-03

* Ajout du support d'autres montres (epix, fr920xt).

### v.1.13 - 2015-12-29

* Ajout d'une option permettant l'affichage de la barre de progression des pas journaliers.
* Ajout d'une option pour changer l'affichage des pas : pas actuels, pas restant, pourcentage de l'objectif ou masqué.

### v.1.12 - 2015-12-28

* Ajout d'une option permettant l'affichage de la barre d'inactivité.

### v.1.11 - 2015-12-25

* Ajout d'une option pour changer l'affichage de la batterie : icône et texte, icône, texte ou masqué.

### v.1.10 - 2015-12-16

* Ajout d'une option pour changer la couleur de la date et autres libellés.

### v.1.9 - 2015-12-15

* Ajout de l'option fond blanc. Le noir est ajouté aux couleurs disponibles pour les heures et minutes.

### v.1.8 - 2015-12-11

* Ajout d'une disposition où les heures et minutes ont la même taille.

### v.1.7 - 2015-12-10

* Ajout d'une option pour accentuer les heures ou les minutes.
* Ajout des langues : italien et espagnol.

### v.1.6 - 2015-12-09

* Correction de la sauvegarde et application des préférences de couleur.

### v.1.0 - 2015-12-04

* Première version publique.
