using Toybox.Application as App;
using Toybox.System as Sys;
using Toybox.WatchUi as Ui;

//!
//! This class represents the the watch face application.
//!
class MercuryApp extends App.AppBase {

	//! Initialize the app
	function initialize() {
		AppBase.initialize();

		// Update app version in preferences
		var version = Ui.loadResource(Rez.Strings.AppVersion);
		setProperty("appVersion", version);
	}

	//! Return the initial view of your application here
	function getInitialView() {
		view = new MercuryView();
		view.loadPreferences();
		return [ view ];
	}

	//! New app settings have been received
	function onSettingsChanged() {
		if (view != null) {
			try {
				view.loadPreferences();
			} catch(ex) {
				// Ignored
			}
		}
		Ui.requestUpdate();
	}


	hidden var view;
}
