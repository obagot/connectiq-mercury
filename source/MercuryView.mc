using Toybox.Application as App;
using Toybox.Graphics as Gfx;
using Toybox.Lang as Lang;
using Toybox.System as Sys;
using Toybox.Time as Time;
using Toybox.ActivityMonitor as ActMon;
using Toybox.WatchUi as Ui;

//!
//! This class represents the main view for the watch face.
//! Vivoactive is 205x148.
//!
class MercuryView extends Ui.WatchFace {

	//! Initialize the view
	function initialize() {
		WatchFace.initialize();
		deviceSettings = Sys.getDeviceSettings();
	}

	//! Load your resources here
	function onLayout(dc) {
		// Dynamic layout based on preferences
		setPreferedLayout(dc);
	}

	//! Called when this View is brought to the foreground.
	//! Restore the state of this View and prepare it to be shown.
	//! This includes loading resources into memory.
	function onShow() {
		// Load strings
		dateFormat = Ui.loadResource(Rez.Strings.DateFormat);
		sleepingText = Ui.loadResource(Rez.Strings.Sleeping);

		// Load bitmaps
		// Bitmaps loaded during preference loading
	}

	//! Update the view
	function onUpdate(dc) {
		// Dynamic layout based on preferences
		setPreferedLayout(dc);

		// Reload settings
		deviceSettings = Sys.getDeviceSettings();

		// Update sleep mode
		sleepMode = ActMon.getInfo().isSleepMode;

		// For FORMAT_SHORT, all values in Info are integers. For FORMAT_MEDIUM and FORMAT_LONG, the month/day fields have the strings for the date in question
		var dateInfo = Time.Gregorian.info(Time.now(), Time.FORMAT_MEDIUM);

		var view;

		// Draw the hour
		view = View.findDrawableById(HourLabel);
		drawHourDigits(dateInfo, view);

		// Draw the minutes
		view = View.findDrawableById(MinuteLabel);
		drawMinuteDigits(dateInfo, view);

		// Draw the seconds
		view = View.findDrawableById(SecondLabel);
		drawSecondDigits(dateInfo, view);

		// Draw the date
		view = View.findDrawableById(DateLabel);
		drawDate(dateInfo, view);

		// Draw battery level
		view = View.findDrawableById(BatteryLevelLabel);
		drawBatteryLevel(view);

		// Draw activity steps
		view = View.findDrawableById(ActivityLabel);
		drawActivity(view);

		// Call the parent onUpdate function to redraw the layout
		View.onUpdate(dc);

		// Additional drawing to display after the parent onUpdate.

		// Battery icon
		if (batteryMode == BATT_MODE_FULL || batteryMode == BATT_MODE_ICON) {
			dc.drawBitmap(179, 6, batteryImage);
		}

		// Step/sleep icon
		if (sleepMode) {
			dc.drawBitmap(2, 6, sleepImage);
		} else {
			if (stepsMode != STEP_MODE_NONE) {
				dc.drawBitmap(2, 6, stepsImage);
			}
		}

		// Align icons
		var posx = 179;
		var posy = 118;

		// Bluetooth icon
		if (deviceSettings.phoneConnected) {
			dc.drawBitmap(posx, posy, bluetoothImage);
			posy -= 26;
		}

		// Alarm icon
		if (deviceSettings.alarmCount > 0) {
			dc.drawBitmap(posx, posy, alarmImage);
			posy -= 26;
		}

		// Notification icon
		if (deviceSettings.notificationCount > 0) {
			dc.drawBitmap(posx, posy, notificationImage);
		}

		// Draw move bar
		if (showMoveBar && !sleepMode) {
			drawMoveBar(dc);
		}

		// Draw steps bar
		if (showStepsBar && !sleepMode) {
			drawStepsBar(dc);
		}
	}

	//! Called when this View is removed from the screen.
	//! Save the state of this View here. This includes freeing resources from memory.
	function onHide() {
		dateFormat = null;

		alarmImage = null;
		batteryImage0 = null;
		batteryImage15 = null;
		batteryImage33 = null;
		batteryImage50 = null;
		batteryImage66 = null;
		batteryImage85 = null;
		batteryImage100 = null;
		batteryImage = null;
		bluetoothImage = null;
		notificationImage = null;
		sleepImage = null;
		stepsImage = null;
	}

	//! The user has just looked at their watch. Timers and animations may be started here.
	function onExitSleep() {
		lowpowerMode = false;
	}

	//! Terminate any active timers and prepare for slow updates.
	function onEnterSleep() {
		lowpowerMode = true;
		Ui.requestUpdate();
	}

	//! Load preferences for the view from the object store.
	//! This can be called from the app when the settings have changed.
	function loadPreferences() {
		var app = App.getApp();

		if (app != null) {
			hoursColor = pref2Color(app.getProperty("hoursColor"), hoursColor);
			minutesColor = pref2Color(app.getProperty("minutesColor"), minutesColor);
			dateColor = pref2Color(app.getProperty("dateColor"), dateColor);
			infoColor = pref2Color(app.getProperty("infoColor"), infoColor);

			layoutMode = pref2LayoutMode(app.getProperty("layoutMode"), layoutMode);
			whiteBackground = pref2Boolean(app.getProperty("whiteBackground"), whiteBackground);
			batteryMode = pref2BatteryMode(app.getProperty("batteryMode"), batteryMode);
			stepsMode = pref2BatteryMode(app.getProperty("stepsMode"), stepsMode);
			showDate = pref2Boolean(app.getProperty("showDate"), showDate);
			showMoveBar = pref2Boolean(app.getProperty("showMoveBar"), showMoveBar);
			showStepsBar = pref2Boolean(app.getProperty("showStepsBar"), showStepsBar);
			hourLeadingZero = pref2Boolean(app.getProperty("hourLeadingZero"), hourLeadingZero);

			loadBitmaps();
		}
	}

	//! Set layout based on preference
	//! @param dc Drawing context
	hidden function setPreferedLayout(dc) {
		// Dynamic layout based on preferences
		if (layoutMode == LAYOUT_LARGE_HOURS) {
			setLayout(whiteBackground ? Rez.Layouts.LargeHoursWhite(dc) : Rez.Layouts.LargeHoursBlack(dc));
		} else if (layoutMode == LAYOUT_LARGE_CLOCK) {
			setLayout(whiteBackground ? Rez.Layouts.LargeClockWhite(dc) : Rez.Layouts.LargeClockBlack(dc));
		} else {
			setLayout(whiteBackground ? Rez.Layouts.LargeMinutesWhite(dc) : Rez.Layouts.LargeMinutesBlack(dc));
		}
	}

	//! Load bitmaps based on background color
	hidden function loadBitmaps() {
		if (whiteBackground) {
			alarmImage = Ui.loadResource(Rez.Drawables.Alarm_B);
			batteryImage0 = Ui.loadResource(Rez.Drawables.Battery0_B);
			batteryImage15 = Ui.loadResource(Rez.Drawables.Battery15_B);
			batteryImage33 = Ui.loadResource(Rez.Drawables.Battery33_B);
			batteryImage50 = Ui.loadResource(Rez.Drawables.Battery50_B);
			batteryImage66 = Ui.loadResource(Rez.Drawables.Battery66_B);
			batteryImage85 = Ui.loadResource(Rez.Drawables.Battery85_B);
			batteryImage100 = Ui.loadResource(Rez.Drawables.Battery100_B);
			batteryImage = batteryImage100;
			bluetoothImage = Ui.loadResource(Rez.Drawables.Bluetooth_B);
			notificationImage = Ui.loadResource(Rez.Drawables.Notification_B);
			stepsImage = Ui.loadResource(Rez.Drawables.Steps_B);
			sleepImage = Ui.loadResource(Rez.Drawables.Sleep_B);
		} else {
			alarmImage = Ui.loadResource(Rez.Drawables.Alarm_W);
			batteryImage0 = Ui.loadResource(Rez.Drawables.Battery0_W);
			batteryImage15 = Ui.loadResource(Rez.Drawables.Battery15_W);
			batteryImage33 = Ui.loadResource(Rez.Drawables.Battery33_W);
			batteryImage50 = Ui.loadResource(Rez.Drawables.Battery50_W);
			batteryImage66 = Ui.loadResource(Rez.Drawables.Battery66_W);
			batteryImage85 = Ui.loadResource(Rez.Drawables.Battery85_W);
			batteryImage100 = Ui.loadResource(Rez.Drawables.Battery100_W);
			batteryImage = batteryImage100;
			bluetoothImage = Ui.loadResource(Rez.Drawables.Bluetooth_W);
			notificationImage = Ui.loadResource(Rez.Drawables.Notification_W);
			stepsImage = Ui.loadResource(Rez.Drawables.Steps_W);
			sleepImage = Ui.loadResource(Rez.Drawables.Sleep_W);
		}
	}

	//! Draw the hour digits
	//! @param [Toybox::Time::Gregorian::Info] dateInfo the current date info
	//! @param [Toybox::WatchUi::View] view the view to draw in
	hidden function drawHourDigits(dateInfo, view) {
		var hour = dateInfo.hour;
		if (!deviceSettings.is24Hour) {
			hour = to12hourFormat(hour);
		}

		if (hourLeadingZero && hour < 10) {
			view.setText(Lang.format("0$1$", [hour]));
		} else {
			view.setText(Lang.format("$1$", [hour]));
		}

		view.setColor(hoursColor);
	}

	//! Draw the minute digits
	//! @param [Toybox::Time::Gregorian::Info] dateInfo the current date info
	//! @param [Toybox::WatchUi::View] view the view to draw in
	hidden function drawMinuteDigits(dateInfo, view) {
		view.setText(to2digitFormat(dateInfo.min));
		view.setColor(minutesColor);
	}

	//! Draw the second digits
	//! @param [Toybox::Time::Gregorian::Info] dateInfo the current date info
	//! @param [Toybox::WatchUi::View] view the view to draw in
	hidden function drawSecondDigits(dateInfo, view) {
		var secondString = "";
		if (!lowpowerMode) {
			//secondString = Lang.format("$1$", [dateInfo.sec.format("%.2d")]);
			secondString = to2digitFormat(dateInfo.sec);
		}

		view.setText(secondString);
		view.setColor(infoColor);
	}

	//! Draw the date
	//! @param [Toybox::Time::Gregorian::Info] dateInfo the current date info
	//! @param [Toybox::WatchUi::View] view the view to draw in
	hidden function drawDate(dateInfo, view) {
		if (showDate) {
			var dateString = Lang.format(dateFormat, [dateInfo.day_of_week, dateInfo.month, dateInfo.day.format("%d")]);
			view.setText(dateString);
			view.setColor(dateColor);
		} else {
			view.setText("");
		}
	}

	//! Draw the battery level
	//! @param [Toybox::WatchUi::View] view the view to draw in
	hidden function drawBatteryLevel(view) {
		var batteryLevel = Sys.getSystemStats().battery;

		if (batteryMode == BATT_MODE_FULL || batteryMode == BATT_MODE_TEXT) {
			view.setText(Lang.format("$1$%", [batteryLevel.format("%d")]));

			// Text color
			if (batteryLevel <= 10) {
				view.setColor(Gfx.COLOR_RED);
			} else if (batteryLevel <= 20) {
				view.setColor(Gfx.COLOR_ORANGE);
			} else {
				view.setColor(infoColor);
			}

			if (batteryMode == BATT_MODE_TEXT) {
				// Remove icon margin
				view.locX = 175 + 24 + 2;
			} else {
				view.locX = 175;
			}
		} else {
			view.setText("");
		}

		// Battery icon
		if (batteryLevel < 10) {
			batteryImage = batteryImage0;
		} else if (batteryLevel < 25) {
			batteryImage = batteryImage15;
		} else if (batteryLevel < 40) {
			batteryImage = batteryImage33;
		} else if (batteryLevel < 60) {
			batteryImage = batteryImage50;
		} else if (batteryLevel < 75) {
			batteryImage = batteryImage66;
		} else if (batteryLevel < 91) {
			batteryImage = batteryImage85;
		} else {
			batteryImage = batteryImage100;
		}
	}

	//! Draw the activity (steps)
	//! @param [Toybox::WatchUi::View] view the view to draw in
	hidden function drawActivity(view) {
		if (!sleepMode) {
			if (stepsMode != STEP_MODE_NONE) {
				var info = ActMon.getInfo();
				var steps = info.steps;
				var goal = info.stepGoal;
				var value = 0;
				var color = infoColor;

				if (stepsMode == STEP_MODE_CURRENT) {
					value = steps;
					color = infoColor;
					if (goal > 0 && steps >= goal) {
						color = whiteBackground ? Gfx.COLOR_DK_GREEN : Gfx.COLOR_GREEN;
					} else {
						color = infoColor;
					}
					value = Lang.format("$1$", [value.format("%d")]);
				} else if (stepsMode == STEP_MODE_REMAIN) {
					if (goal > 0 && steps >= goal) {
						value = steps - goal;
						color = whiteBackground ? Gfx.COLOR_DK_GREEN : Gfx.COLOR_GREEN;
					} else {
						value = goal - steps;
						color = infoColor;
					}
					value = Lang.format("$1$", [value.format("%d")]);
				} else if (stepsMode == STEP_MODE_PERCENT) {
					value = steps * 100 / goal;
					if (value >= 100.0) {
						color = whiteBackground ? Gfx.COLOR_DK_GREEN : Gfx.COLOR_GREEN;
					} else {
						color = infoColor;
					}
					value = Lang.format("$1$%", [value.format("%d")]);
				}

				view.setText(value);
				view.setColor(color);
			} else {
				view.setText("");
			}
		} else {
			view.setText(sleepingText);
			view.setColor(infoColor);
		}
	}

	//! Draw the move bar.
	//! @param [Toybox::Graphics::Dc] dc the drawing context
	hidden function drawMoveBar(dc) {
		var moveLevel = ActMon.getInfo().moveBarLevel;

		var color = whiteBackground ? Gfx.COLOR_DK_RED : Gfx.COLOR_RED;
		dc.setColor(color, color);

		if (moveLevel > 0) {
			var x = 5, y = 144, size = 93;
			dc.drawLine(x, y, x + size, y);
			x -= 1; y += 1;
			dc.drawLine(x, y, x + size, y);
			x -= 1; y += 1;
			dc.drawLine(x, y, x + size, y);
		}
		if (moveLevel > 1) {
			var x = 103, y = 144, size = 21;
			dc.drawLine(x, y, x + size, y);
			x -= 1; y += 1;
			dc.drawLine(x, y, x + size, y);
			x -= 1; y += 1;
			dc.drawLine(x, y, x + size, y);
		}
		if (moveLevel > 2) {
			var x = 129, y = 144, size = 21;
			dc.drawLine(x, y, x + size, y);
			x -= 1; y += 1;
			dc.drawLine(x, y, x + size, y);
			x -= 1; y += 1;
			dc.drawLine(x, y, x + size, y);
		}
		if (moveLevel > 3) {
			var x = 155, y = 144, size = 21;
			dc.drawLine(x, y, x + size, y);
			x -= 1; y += 1;
			dc.drawLine(x, y, x + size, y);
			x -= 1; y += 1;
			dc.drawLine(x, y, x + size, y);
		}
		if (moveLevel > 4) {
			var x = 181, y = 144, size = 21;
			dc.drawLine(x, y, x + size, y);
			x -= 1; y += 1;
			dc.drawLine(x, y, x + size, y);
			x -= 1; y += 1;
			dc.drawLine(x, y, x + size, y);
		}
	}

	//! Draw the steps bar.
	//! Vivoactive is 205x148.
	//! @param [Toybox::Graphics::Dc] dc the drawing context
	hidden function drawStepsBar(dc) {
		var info = ActMon.getInfo();
		var steps = info.steps;
		var goal = info.stepGoal;
		var percent = steps * 100 / goal;

		if (percent <= 100) {
			var x = 5, y = 1;
			var size = 198 * percent / 100;

			var color = whiteBackground ? Gfx.COLOR_DK_GRAY : Gfx.COLOR_LT_GRAY;
			dc.setColor(color, color);

			dc.drawLine(x, y, x + size, y);
			x -= 1; y += 1;
			dc.drawLine(x, y, x + size, y);
			x -= 1; y += 1;
			dc.drawLine(x, y, x + size, y);
		} else {
			// First part
			var x = 5, y = 1;
			var size = 198 * goal / steps;

			var color = whiteBackground ? Gfx.COLOR_DK_GRAY : Gfx.COLOR_LT_GRAY;
			dc.setColor(color, color);

			dc.drawLine(x, y, x + size, y);
			x -= 1; y += 1;
			dc.drawLine(x, y, x + size, y);
			x -= 1; y += 1;
			dc.drawLine(x, y, x + size, y);

			// Second part
			x = 5 + size; y = 1;
			size = 198 * (steps - goal) / steps;

			color = whiteBackground ? Gfx.COLOR_DK_GREEN : Gfx.COLOR_GREEN;
			dc.setColor(color, color);

			dc.drawLine(x, y, x + size, y);
			x -= 1; y += 1;
			dc.drawLine(x, y, x + size, y);
			x -= 1; y += 1;
			dc.drawLine(x, y, x + size, y);
		}
	}

	//! Convert 24 hour format to 12 hour format
	//! @param hour the hour to convert
	hidden function to12hourFormat(hour) {
		var hour12 = hour % 12;
		if (hour12 == 0) {
			hour12 = 12;
		}
		return hour12;
	}

	//! Format number with 2 digits
	//! Bug: 2-digit format does not work on productive watch (1.2.1)
	//! @param number the number to format
	hidden function to2digitFormat(number) {
		// var minuteString = Lang.format("$1$", [minutes.format("%.2d")]);
		var string;
		if (number == 0) {
			string = "00";
		} else if (number < 10) {
			string = Lang.format("0$1$", [number.format("%.d")]);
		} else {
			string = Lang.format("$1$", [number.format("%.d")]);
		}
		return string;
	}

	//! Return the color code for the preference value
	//! @param pref the preference value (should be number, but GCM returns string)
	//! @param def the default value if preference value cannot be found
	hidden function pref2Color(pref, def) {
		try {
			if (pref != null) {
				// GCM returns value as string!
				if (pref instanceof Toybox.Lang.String) {
					try {
						pref = pref.toNumber();
					} catch(ex1) {
						return def;
					}
				}

				if (pref >= 0 && pref < COLOR_VALUE.size()) {
					return COLOR_VALUE[pref];
				}
			}
		} catch(ex) {
			return def;
		}

		// Default
		return def;
	}

	//! Return the layout mode for the preference value
	//! @param pref the preference value (should be number, but GCM returns string)
	//! @param def the default value if preference value cannot be found
	hidden function pref2LayoutMode(pref, def) {
		try {
			if (pref != null) {
				// GCM returns value as string!
				if (pref instanceof Toybox.Lang.String && pref != "") {
					try {
						pref = pref.toNumber();
					} catch(ex1) {
						return def;
					}
				}

				if (pref == 1) {
					return LAYOUT_LARGE_HOURS;
				} else if (pref == 2) {
					return LAYOUT_LARGE_CLOCK;
				}
			}
		} catch(ex) {
			return def;
		}

		// Default
		return def;
	}

	//! Return the battery mode for the preference value
	//! @param pref the preference value (should be number, but GCM returns string)
	//! @param def the default value if preference value cannot be found
	hidden function pref2BatteryMode(pref, def) {
		try {
			if (pref != null) {
				// GCM returns value as string!
				if (pref instanceof Toybox.Lang.String && pref != "") {
					try {
						pref = pref.toNumber();
					} catch(ex1) {
						return def;
					}
				}

				if (pref == 1) {
					return BATT_MODE_ICON;
				} else if (pref == 2) {
					return BATT_MODE_TEXT;
				} else if (pref == 3) {
					return BATT_MODE_NONE;
				}
			}
		} catch(ex) {
			return def;
		}

		// Default
		return def;
	}

	//! Return the boolean value for the preference
	//! @param pref the preference value, should be a Boolean, but GCM returns a String
	//! @param def the default value if preference value cannot be found
	hidden function pref2Boolean(pref, def) {
		try {
			if (pref != null) {
				if (pref instanceof Toybox.Lang.Boolean) {
					return pref;
				}

				// GCM returns value as string!
				if (pref instanceof Toybox.Lang.String) {
					try {
						pref = pref.toBoolean();
						return pref;
					} catch(ex1) {
						return def;
					}
				}

				if (pref == 1) {
					return true;
				}
			}
		} catch(ex) {
			return def;
		}

		// Default
		return def;
	}


	//! List of layouts
	enum {
		LAYOUT_LARGE_MINUTES = 0,
		LAYOUT_LARGE_HOURS = 1,
		LAYOUT_LARGE_CLOCK = 2
	}

	//! List of battery modes
	enum {
		BATT_MODE_FULL = 0,
		BATT_MODE_ICON = 1,
		BATT_MODE_TEXT = 2,
		BATT_MODE_NONE = 3
	}

	//! List of steps modes
	enum {
		STEP_MODE_CURRENT = 0,
		STEP_MODE_REMAIN = 1,
		STEP_MODE_PERCENT = 2,
		STEP_MODE_NONE = 3
	}

	//! List of Graphics color values equivalence for preferences.
	//! Table with index 0 to 12, see settings.xml
	hidden var COLOR_VALUE = [
		Gfx.COLOR_WHITE,
		Gfx.COLOR_LT_GRAY,
		Gfx.COLOR_DK_GRAY,
		Gfx.COLOR_RED,
		Gfx.COLOR_DK_RED,
		Gfx.COLOR_ORANGE,
		Gfx.COLOR_YELLOW,
		Gfx.COLOR_GREEN,
		Gfx.COLOR_DK_GREEN,
		Gfx.COLOR_BLUE,
		Gfx.COLOR_DK_BLUE,
		Gfx.COLOR_PURPLE,
		Gfx.COLOR_PINK,
		Gfx.COLOR_BLACK
	];


	hidden var deviceSettings;
	hidden var lowpowerMode = false;
	hidden var sleepMode = false;

	// Preferences
	hidden var layoutMode = LAYOUT_LARGE_MINUTES;
	hidden var hoursColor = Gfx.COLOR_WHITE;
	hidden var minutesColor = Gfx.COLOR_WHITE;
	hidden var dateColor = Gfx.COLOR_LT_GRAY;
	hidden var infoColor = Gfx.COLOR_LT_GRAY;
	hidden var whiteBackground = false;
	hidden var batteryMode = BATT_MODE_FULL;
	hidden var stepsMode = STEP_MODE_CURRENT;
	hidden var showDate = true;
	hidden var showMoveBar = false;
	hidden var showStepsBar = false;
	hidden var hourLeadingZero = false;

	// Cached strings
	hidden var dateFormat;
	hidden var sleepingText;

	// Images
	hidden var alarmImage;
	hidden var batteryImage;
	hidden var batteryImage0;
	hidden var batteryImage15;
	hidden var batteryImage33;
	hidden var batteryImage50;
	hidden var batteryImage66;
	hidden var batteryImage85;
	hidden var batteryImage100;
	hidden var bluetoothImage;
	hidden var notificationImage;
	hidden var sleepImage;
	hidden var stepsImage;

	// UI field names
	hidden const HourLabel = "HourLabel";
	hidden const MinuteLabel = "MinuteLabel";
	hidden const SecondLabel = "SecondLabel";
	hidden const DateLabel = "DateLabel";
	hidden const BatteryLevelLabel = "BatteryLevelLabel";
	hidden const ActivityLabel = "ActivityLabel";

}
