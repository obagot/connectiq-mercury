# Mercury #

## Description ##

Mercury is a watch face for Garmin watches.

It is designed as a clear and intuitive interface, featuring move bar, battery status, step count, phone connection, alarm and notification indicators.

Hour and minute digit colors are configurable through Garmin Connect Mobile.

Configurable layout enables emphasizing either hour or minute digits.

Download from Garmin's [Connect IQ Store](https://apps.garmin.com/en-US/apps/bd260ed0-21db-43f3-97f1-0e0c6fc38b9a).

Supported languages: English, French, Italian, Spanish.

Supported models: vívoactive®, epix™, Forerunner® 920XT.

![Capture](dist/images/capture0.png)
![Capture](dist/images/capture5.png)
![Capture](dist/images/capture7.png)

## History ##

Mercury is tested on Garmin vívoactive running Connect IQ 1.2.3.

### v.1.17 - 2016-02-04

* Fixed preferences settings when some preferences are not set.

### v.1.16 - 2016-02-02

* Fixed crash on preference loading.

### v.1.15 - 2016-02-02

* Added separate color options for the date and the other information.
* Added option to hide the date.
* Added option to show leading zero in hour.
* In sleep mode, steps bar and move bar are now hidden.
* Changed battery color thresholds: orange < 20%, red < 10%.
* Upgrade to Garmin Connect IQ SDK 1.2.3.

### v.1.14 - 2016-01-03

* Added support for other square watches (epix, fr920xt).

### v.1.13 - 2015-12-29

* Added option to display steps progress bar.
* Added option to change step display mode: current steps, remaining steps, percentage of objective or none.

### v.1.12 - 2015-12-28

* Added option to display move bar (inactivity level).

### v.1.11 - 2015-12-25

* Added option to change battery display mode: icon, text, both or none.

### v.1.10 - 2015-12-16

* Added option to change color for date and other labels.

### v.1.9 - 2015-12-15

* Added option for a white background. Black is added to the list of digit colors.

### v.1.8 - 2015-12-11

* Added a layout where hours and minutes have the same size.

### v.1.7 - 2015-12-10

* Added option to emphasize hour or minute digits.
* Added support for Italian and Spanish.

### v.1.6 - 2015-12-09

* Fixed color preferences save/restore.

### v.1.0 - 2015-12-04

* First public release.